const { WHITE_LISTS } = require("./config");

const whitelist = WHITE_LISTS.split(",");

module.exports = whitelist;
