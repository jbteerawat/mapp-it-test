module.exports = (fn) => {
  return (req, res, next) => {
    // fn(req, res, next).catch(next)
    fn(req, res, next).catch((error) => {
      console.log(error);
      const status = error.status ?? 500;
      return res.status(status).json({
        error: {
          code: error.code,
          message: error.message,
        },
      });
    });
  };
};
