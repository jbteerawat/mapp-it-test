const AppError = require("../utils/app-error");
const jwt = require("jsonwebtoken");
const { format, addYears } = require("date-fns");
const {
  EXPIRED_TOKEN,
  IS_DATETIME_FORMAT_TH,
  JWT_KEY,
  FORMAT_DATE,
  FORMAT_DATE_TH,
  FORMAT_TIME,
  IS_SHORT_YEAR,
  PRODUCTS,
  GCP_BUCKET_NAME,
  MONTHS_TH,
  MONTHS_EN,
  SHORT_MONTHS_TH,
  SHORT_MONTHS_EN,
} = require("../config");
const { LANGS } = require(`../config`);

const random = (digit = 10) => {
  let x = 1;
  for (let i = 0; i < digit; i++) {
    x *= 10;
  }
  let rand = 0;
  let lengthRandom = 0;
  do {
    rand = Math.floor(Math.random() * x);
    lengthRandom = rand.toString().length;
  } while (lengthRandom !== digit || rand === 0);

  return rand;
};

const randomBetween = (min = 1000000000, max = 9999999999) => {
  return Math.floor(Math.random() * (max - min + 1)) + min;
};

const jwtEncode = (dataObject, expiredToken) => {
  try {
    const token = jwt.sign(dataObject, JWT_KEY, {
      expiresIn: expiredToken || EXPIRED_TOKEN,
    });
    return token;
  } catch (error) {
    return null;
  }
};

const jwtDecode = async (token) => {
  return new Promise(function (resolve, reject) {
    try {
      // executor (the producing code, "singer")
      jwt.verify(token, JWT_KEY, (err, decoded) => {
        if (err) {
          if (err.message.includes("jwt expired")) {
            reject(new AppError("token-expired", "Token is expired!"));
          } else if (err.message.includes("invalid signature")) {
            reject(new AppError("invalid-token", "Invalid token!"));
          } else {
            reject(new AppError("invalid-token", "Invalid token!"));
          }
        } else {
          resolve(decoded);
        }
      });
    } catch (error) {
      reject(new AppError("decode-token-error", error.message));
    }
  });
};

const numberWithCommas = (x, digit = 0) => {
  if (!x || !Number(x)) return (0).toFixed(digit).toString();
  x = x.toFixed(digit);
  var parts = x.toString().split(".");
  parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  return parts.join(".");
};

const sText = (string) => {
  let name = string.charAt(0).toLowerCase() + string.slice(1);

  const endChar = name.charAt(name.length - 1);
  if (
    name.endsWith("ay") ||
    name.endsWith("ey") ||
    name.endsWith("iy") ||
    name.endsWith("oy") ||
    name.endsWith("uy")
  ) {
    name = name + "s";
  } else if (endChar === "y") {
    name = name.slice(0, -1) + "ies";
  } else if (
    name.endsWith("s") ||
    name.endsWith("sh") ||
    name.endsWith("ch") ||
    name.endsWith("x") ||
    name.endsWith("z") ||
    name.endsWith("o") ||
    name.endsWith("ss")
  ) {
    name = name + "es";
  } else {
    name += "s";
  }
  return name;
};

const getLastDayOfMonth = (date = new Date()) => {
  const y = date.getFullYear();
  const m = date.getMonth();
  const lastday = new Date(y, m + 1, 0).getDate();
  return lastday;
};

const timeAgo = (datetime = new Date()) => {
  return formatDistance(datetime, new Date(), {
    addSuffix: true,
  });
};

const dateFormat = (options = {}) => {
  let {
    date = new Date(),
    setFormat,
    isShortYear = IS_SHORT_YEAR,
    isDatetimeFormatTH = IS_DATETIME_FORMAT_TH,
  } = options;

  if (!date) return null;

  let newFormat = "";
  if (setFormat) {
    newFormat = format(date, setFormat);
  }

  if (!setFormat) {
    if (isDatetimeFormatTH) {
      let year = (date.getFullYear() + 543).toString();
      if (isShortYear) {
        year = year.substring(2, 4);
      }

      const month = date.getMonth() + 1;
      const dt = date.getDate();
      let fmt = FORMAT_DATE_TH;

      fmt = fmt.replace("yyyy", year);
      fmt = fmt.replace("yy", year);

      fmt = fmt.replace("MM", pad(month));
      fmt = fmt.replace("dd", pad(dt));
      return fmt;
    } else {
      if (isShortYear) {
        setFormat = FORMAT_DATE.replace("yyyy", "yy");
      } else {
        setFormat = FORMAT_DATE;
      }
      newFormat = format(date, setFormat);
    }
  } else {
    if (isDatetimeFormatTH) {
      let year = (date.getFullYear() + 543).toString();
      if (isShortYear) {
        year = year.substring(2, 4);
      }
      setFormat = setFormat.replace("yyyy", "");
      setFormat = setFormat.replace("yy", "");
      newFormat = format(date, setFormat);
      newFormat += year;
    }
  }

  return newFormat;
};

const timeFormat = (options = {}) => {
  let { date = new Date(), setFormat } = options;

  if (!setFormat) {
    setFormat = FORMAT_TIME;
  }

  const newFormat = format(date, setFormat);
  return newFormat;
};

const datetimeFormat = (options = {}) => {
  let { date = new Date(), setFormat } = options;

  if (!setFormat) {
    const dt = dateFormat({ date });
    const time = timeFormat({ date });

    return `${dt} ${time}`;
  }

  const newFormat = format(newDate, setFormat);
  return newFormat;
};

const pad = (n, unit = 2) => {
  if ((unit <= 1 || !n) && n !== 0) {
    return n ? n.toString() : "";
  } else {
    if (unit <= 0) return n.toString();
    let fullUnit = 1;
    for (let i = 1; i <= unit - 1; i++) {
      fullUnit *= 10;
    }
    return n < fullUnit
      ? "0".repeat(unit - n.toString().length) + n
      : n.toString();
  }
};

const capitalizeFirstLetter = (string) => {
  return string.charAt(0).toUpperCase() + string.slice(1);
};

const getSlug = ({ id, name, datetime = new Date() }) => {
  if (!name || !id) return "no_slug";

  name = name.trim();

  // block => /, ?, #, =, %, {, }
  name = name.replace(/\/|\?|#|=|%|{|}/gi, "-");

  const split = name.split(" ");

  let noneSpace = split.filter((v) => v);

  let joinName = noneSpace.join("-").toLowerCase();

  if (joinName.startsWith("-")) {
    joinName = joinName.substring(1);
  }

  if (joinName.endsWith("-")) {
    joinName = joinName.substring(0, joinName.length - 1);
  }

  joinName = joinName.replace(/--+/g, "-");

  let subName = joinName.substring(0, PRODUCTS.SLUG_NAME_LENGTH);

  const time = datetime.getTime();
  const slug = `${subName}-i.${id}-t.${time}`;

  return slug;
};

const isImageType = async (files) => {
  if (!files) {
    return false;
  }

  if (typeof files === "string") {
    const isImage = await processImageType(files);
    if (isImage) {
      return true;
    }
  } else if (Array.isArray(files)) {
    let countIsArray = 0;
    for (let i = 0; i < files.length; i++) {
      const isImage = await processImageType(files[i]);
      if (isImage) {
        countIsArray++;
      }
    }

    if (countIsArray === files.length) {
      return true;
    }
  } else if (typeof files === "object") {
    if (files.filename) {
      const isImage = await processImageType(files.filename);
      if (isImage) {
        return true;
      }
    }
  }

  return false;
};

const processImageType = (file) => {
  if (
    file.endsWith(".jpg") ||
    file.endsWith(".jpeg") ||
    file.endsWith(".tif") ||
    file.endsWith(".tiff") ||
    file.endsWith(".bmp") ||
    file.endsWith(".gif") ||
    file.endsWith(".png") ||
    file.endsWith(".eps") ||
    file.endsWith(".webp")
  ) {
    return true;
  }

  return false;
};

const reduceDuplicate = (array) => {
  if (!array) return array;
  for (const [index, obj] of array.entries()) {
    if (
      array.filter((x) => JSON.stringify(x) === JSON.stringify(obj)).length > 1
    ) {
      array.splice(index, 1);
    }
  }
  return array;
};

const reduceDuplicateByField = (array = [], field = "") => {
  if (!array || !field) return array;
  const newArray = [];
  for (const [index, obj] of array.entries()) {
    const objField = obj[field];
    const length = newArray.filter((x) => x[field] === objField).length;
    if (!length) {
      newArray.push(obj);
    }
  }
  return newArray;
};

const cutTextStartsWith = ({ text, cut }) => {
  if (!text || !cut) {
    return null;
  }

  if (text.startsWith(cut)) {
    text = text.substring(cut.length, text.length);
  }

  return text;
};

const cutTextEndsWith = ({ text, cut }) => {
  if (!text || !cut) {
    return null;
  }

  if (text.endsWith(cut)) {
    text = text.substring(0, text.length - cut.length);
  }

  return text;
};

const isEmpty = (obj) => {
  for (var prop in obj) {
    if (Object.prototype.hasOwnProperty.call(obj, prop)) {
      return false;
    }
  }

  return JSON.stringify(obj) === JSON.stringify({});
};

const generatePassword = () => {
  var length = 5,
    charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789",
    retVal = "";
  for (var i = 0, n = charset.length; i < length; ++i) {
    retVal += charset.charAt(Math.floor(Math.random() * n));
  }
  return retVal;
};

const uploadImage = ({ file, path = null, rootFolder = "images" }) =>
  new Promise((resolve, reject) => {
    const { originalname, buffer } = file;

    // console.log("file__ =>", file);

    const fileName = `${Date.now()}.${random(10)}.${originalname}`;

    if (path) {
      if (path.startsWith("/")) {
        path = path.slice(1, path.length);
      }

      if (!path.endsWith("/")) {
        path += "/";
      }
    }

    if (rootFolder.startsWith("/")) {
      rootFolder = rootFolder.slice(1, rootFolder.length);
    }

    if (rootFolder.endsWith("/")) {
      rootFolder = rootFolder.slice(0, rootFolder.length - 1);
    }

    path = `${rootFolder}/${path}`;

    // console.log("path =>", path);

    const blob = bucket.file(`${path}${fileName.replace(/ /g, "_")}`);
    // console.log("blob==>", blob.name);
    const blobStream = blob.createWriteStream({
      resumable: false,
    });
    blobStream
      .on("finish", () => {
        // const publicUrl = format(
        //   `https://storage.googleapis.com/${bucket.name}/${blob.name}`
        // );
        const publicUrl = `https://storage.googleapis.com/${bucket.name}/${blob.name}`;
        resolve(publicUrl);
      })
      .on("error", (error) => {
        console.log("upload file error =>", error);
        // reject(`error =>`, error.message);
        reject(`Unable to upload image, something went wrong`);
      })
      .end(buffer);
  });

function deg2rad(deg) {
  return deg * (Math.PI / 180);
}

function getDistanceFromLatLonInKm(lat1, lon1, lat2, lon2) {
  if (!lat1 || !lon1 || !lat2 || !lon2) return 0;

  var R = 6371; // Radius of the earth in km
  var dLat = deg2rad(lat2 - lat1); // deg2rad below
  var dLon = deg2rad(lon2 - lon1);
  var a =
    Math.sin(dLat / 2) * Math.sin(dLat / 2) +
    Math.cos(deg2rad(lat1)) *
      Math.cos(deg2rad(lat2)) *
      Math.sin(dLon / 2) *
      Math.sin(dLon / 2);
  var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
  var d = R * c; // Distance in km
  return d;
}

const sort = (array, field, orderBy = "asc") => {
  const compare = (a, b) => {
    if (a[field] < b[field]) {
      return orderBy === 1 || orderBy === "asc" ? -1 : 1;
    }
    if (a[field] > b[field]) {
      return orderBy === 1 || orderBy === "asc" ? 1 : -1;
    }
    return 0;
  };

  array.sort(compare);
};

const getThaiDate = ({
  date = new Date(),
  isTime = false,
  isShortMonth = false,
  lang = LANGS.TH,
}) => {
  const month = date.getMonth();
  let formatMonth = isShortMonth ? SHORT_MONTHS_TH[month] : MONTHS_TH[month];

  if (lang === LANGS.EN) {
    formatMonth = isShortMonth ? SHORT_MONTHS_EN[month] : MONTHS_EN[month];
  }

  let formatDate = `${date.getDate()} ${formatMonth} ${
    date.getFullYear() + 543
  }`;
  if (isTime) {
    formatDate += ` ${timeFormat({ date })}`;
  }
  return formatDate;
};

const splitText = (text, splitWith = ",") => {
  if (!text) return [];
  const arr = text.split(splitWith);
  const newArr = [];
  for (const txt of arr) {
    const value = txt.trim();
    if (!value) continue;
    newArr.push(value);
  }
  return newArr;
};

const getFilterText = (filter = "") => {
  const ftr = filter && filter.trim() ? filter.trim() : "";
  const result =
    ftr && ftr.includes(",")
      ? splitText(ftr)
      : ftr && ftr.toLowerCase() !== "all"
      ? [ftr]
      : "all";
  return result;
};

// convert json body to form data
const convertFormData = (body) => {
  return new URLSearchParams(body);
};

const isEmptyObject = (obj) => {
  if (
    !obj ||
    (obj &&
      Object.keys(obj).length === 0 &&
      Object.getPrototypeOf(obj) === Object.prototype)
  ) {
    return true;
  }

  return false;
};

module.exports = {
  random,
  randomBetween,
  jwtEncode,
  jwtDecode,
  numberWithCommas,
  sText,
  getLastDayOfMonth,
  timeAgo,
  pad,
  dateFormat,
  timeFormat,
  datetimeFormat,
  capitalizeFirstLetter,
  getSlug,
  isImageType,
  reduceDuplicate,
  reduceDuplicateByField,
  cutTextStartsWith,
  cutTextEndsWith,
  isEmpty,
  generatePassword,
  uploadImage,
  getDistanceFromLatLonInKm,
  sort,
  getThaiDate,
  splitText,
  getFilterText,
  convertFormData,
  isEmptyObject,
};
