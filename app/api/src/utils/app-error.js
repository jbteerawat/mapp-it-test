class AppError extends Error {
  constructor(code, message, status) {
    super(message);
    this.code = code ?? "app-error";
    this.status = status ?? 500;
    this.time = new Date();
  }
}

module.exports = AppError;
