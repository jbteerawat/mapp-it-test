const CB = require("../helpers/CB");
const AppError = require("../utils/app-error");
const { jwtDecode } = require("../helpers/functions");

// middleware
const isLoggedIn = CB(async (req, res, next) => {
  // req.headers.authorization // Bearer <TOKEN>

  const headers = req.headers.authorization;
  const token = headers && headers.split(" ")[1];

  // console.log("headers =>", headers);
  // console.log("token =>", token);

  // console.log("check type =>", typeof token);

  if (!token || token === "null") {
    throw new AppError("not-found-token", "Not found Token!", 401);
  }

  const decodeToken = await jwtDecode(token);
  if (decodeToken) {
    const { userId } = decodeToken;

    if (userId) {
      // const user = await User.findById(userId).populate(
      //   "permission",
      //   "name code"
      // );

      if (user) {
        req.user = { _id: 1 };
        next();
      } else {
        throw new AppError(
          "token-not-found-user",
          "Token not found user!",
          401
        );
      }
    } else {
      throw new AppError("not-found-userid", "Not found user ID!", 401);
    }
  } else {
    throw new AppError("invalid-token", "Invalid token!", 401);
  }
});

const validateToken = CB(async (req, res, next) => {
  const { token } = req.query;

  if (!token) {
    throw new AppError("not-found-token", "Not found Token!", 401);
  }

  const decodeToken = await jwtDecode(token);
  if (decodeToken) {
    const { userId } = decodeToken;

    if (userId) {
      // const user = await User.findById(userId).populate(
      //   "permission",
      //   "name code"
      // );
      const user = { _id: 1 };

      if (user.rememberToken === token || true) {
        req.token = token;
        req.user = user;
        next();
      } else {
        throw new AppError("token-not-match", "Token is not match!", 401);
      }
    } else {
      throw new AppError("not-found-userid", "Not found user ID!", 401);
    }
  } else {
    throw new AppError("invalid-token", "Invalid token!", 401);
  }
});

const isValidToken = CB(async (req, res, next) => {
  const { token } = req.query;

  if (!token) {
    throw new AppError("not-found-token", "Not found Token!", 401);
  }

  const decodeToken = await jwtDecode(token);
  if (decodeToken) {
    const { userId } = decodeToken;

    if (userId) {
      const user = await User.findById(userId).populate(
        "permission",
        "name code"
      );

      if (user) {
        req.token = token;
        req.user = user;
        next();
      } else {
        throw new AppError(
          "token-not-found-user",
          "Token not found user!",
          401
        );
      }
    } else {
      throw new AppError("not-found-userid", "Not found user ID!", 401);
    }
  } else {
    throw new AppError("invalid-token", "Invalid token!", 401);
  }
});

module.exports = {
  isLoggedIn,
  validateToken,
  isValidToken,
};
