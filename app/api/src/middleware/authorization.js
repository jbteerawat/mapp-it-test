// middleware for doing role-based permissions
const permit = (...permittedRoles) => {
  // return a middleware
  return (request, response, next) => {
    const { user } = request;

    if (
      user &&
      user.permission &&
      permittedRoles.includes(user.permission.code)
    ) {
      next(); // role is allowed, so continue on the next middleware
    } else {
      return response.status(403).json({
        error: {
          code: "forbidden",
          message: "You have been denied permission to acceses this request!",
        },
      }); // user is forbidden
    }
  };
};

module.exports = { permit };
