const CB = require("../helpers/CB");
const AppError = require("../utils/app-error");
const { numberWithCommas } = require("../helpers/functions");

const index = CB(async (req, res, next) => {
  let { memberCount } = req.params;

  // memberCount validator
  if (!memberCount || isNaN(memberCount) || memberCount % 1 !== 0)
    throw new AppError("member-count-invalid", "Please enter a memberCount!");

  if (memberCount <= 0 || memberCount > 100)
    throw new AppError(
      "member-count-range-invalid",
      "Please enter a member-count between 1-100!"
    );

  memberCount = parseInt(memberCount);

  // process
  const sequence = [0];
  let round = 1;
  let fn1 = 0;
  let fn2 = 1;
  let total = 0;

  do {
    const sum = fn1 + fn2;
    total += sum;
    sequence.push(sum);

    fn2 = fn1;
    fn1 = sum;
    round++;
  } while (round < memberCount);

  return res.json({
    "member-count": memberCount,
    sequence,
    total,
  });
});

module.exports = {
  index,
};
