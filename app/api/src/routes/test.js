const express = require("express");
const controller = require("../controllers/test-controller");
const { isLoggedIn } = require("../middleware/verify");
const { permit } = require("../middleware/authorization");

const router = express.Router();

router.route("/:memberCount").get(controller.index);

module.exports = router;
