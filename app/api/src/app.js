const express = require("express");
const morgan = require("morgan");
const rateLimit = require("express-rate-limit");
const mongoSanitize = require("express-mongo-sanitize");
const xss = require("xss-clean");
const app = require("./config/app");
const cors = require("cors");
const whitelist = require("./cors-whitelist");
const bodyParser = require("body-parser");

const {
  IS_PRODUCTION,
  IS_ENABLED_CORS,
  IS_ENABLED_REQUEST_LIMIT,
  PORT,
  START_PATH,
} = require("./config");

require("dotenv").config();

const corsOptions = {
  origin: function (origin, callback) {
    var originIsWhitelisted =
      whitelist.indexOf(origin) !== -1 || !IS_ENABLED_CORS;
    callback(null, originIsWhitelisted);
  },
  credentials: true,
};

app.use(cors(corsOptions));

// Development logging
if (!IS_PRODUCTION) {
  app.use(morgan("dev"));
}

// Limit requests from same API
// if (IS_ENABLED_REQUEST_LIMIT) {
//   const limiter = rateLimit({
//     windowMs: 15 * 60 * 1000, // 15 minutes
//     max: 100, // limit each IP to 100 requests per windowMs
//     message: "Too many requests from this IP, please try again in 15 minutes!",
//   });

//   app.use("/api", limiter);
// }

// Popular data format
// 1. application/x-www-form-urlencoded
// 2. application/json
// 3. multipart/form-data

// // Body parser, reading data from body into req.body
// // for parsing application/json
// app.use(express.json());

// // Decode Form URL Encoded data
// // for parsing application/x-www-form-urlencoded
// app.use(express.urlencoded({ extended: true }));

app.disable("x-powered-by");
// app.use(multerMid.single('file'))
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

// access files path
app.use(`${START_PATH}/static/images`, express.static("public/images"));
app.use(`${START_PATH}/static/files`, express.static("public/files"));

// Data sanitization against NoSQL query injection
app.use(mongoSanitize());

// Data sanitization against XSS
app.use(xss());

if (START_PATH !== "/") {
  app.get("/", function (req, res, next) {
    return res.json({
      status: "OK",
      message: `Please ensure you enter correct url with ${START_PATH}`,
    });
  });
}

app.get(START_PATH, function (req, res, next) {
  return res.json({
    status: "OK",
    message: "Server is listening on port *:" + PORT,
  });
});

app.get("/health", function (_, res) {
  return res.json({
    status: "OK",
    message: "Server is running",
  });
});

// api
app.use("/api/v1", require("./routes"));

// 404 handler
// app.all('*', (req, res, next) => {
//   return res.json({
//     error: {
//       message: `Can't find ${req.originalUrl} on this server!`,
//     },
//   })
//   // next(new AppError(`Can't find ${req.originalUrl} on this server!`, 404))
// })

// app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));
module.exports = app;
