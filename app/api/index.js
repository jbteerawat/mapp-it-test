const { PORT } = require("./src/config");
const server = require("./src/server");

require("./src/app");

server.listen(PORT, () => {
  console.log(`Server is listening on port *:${PORT}`);
});

module.exports = server;
