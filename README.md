# mapp-it-test

## How to run

```
npm install -g yarn (only for the first time)
```

Go to directory app/api and run:

```
yarn install
```

```
yarn dev
```

Server is listening on port \*:3001 http://localhost:3001

## How to test

You can test at this endpoint http://localhost:3001/api/v1/test/10

**_Explain more:_**<br />
http://localhost:3001/api/v1/test/x <=== x is the meaning of member count you can enter a number between 1-100

**_Please attention:_**<br />
About the logic with Fibonacci numbers, the sequence numbers will start with 0, 1, 1, x, x, ...<br />
I use reference follow by https://en.wikipedia.org/wiki/Fibonacci_number and https://www.mathsisfun.com/numbers/fibonacci-sequence.html

## How to build

Go to directory app/api and run:

```
docker login registry.gitlab.com
```

```
docker build --platform linux/amd64 -t registry.gitlab.com/jbteerawat/mapp-it-test/test:latest .
```

```
docker push registry.gitlab.com/jbteerawat/mapp-it-test/test:latest
```

And then run:

```
docker run --name test -d -p 3001:3001 registry.gitlab.com/jbteerawat/mapp-it-test/test:latest
```

## How to setup Grafana and Prometheus on server:

- Pull the project git on server
- Go to directory /root/mapp-it-test/monitor
- Run docker-compose up -d
- Done.
